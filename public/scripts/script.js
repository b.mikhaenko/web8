if (document.readyState !== "loading") {
    onReady();
} else {
    document.addEventListener("DOMContentLoaded", onReady);
}


function onReady() {
    window.history.pushState({ popup: "close" }, "popup", "./");
    window.addEventListener("popstate", function (e) {
        if (e.state.popup === "close") {
            closePopUp();
        }
        if (e.state.popup === "open") {
            openPopUp();
        }
    });

    let button = document.getElementById("auth-popup-start");
    button.addEventListener("click", openPopUp);
    let closeSpan = document.getElementsByClassName("close")[0];
    closeSpan.addEventListener("click", closePopUp);
    let userName = document.getElementById("user-name");
    let userEmail = document.getElementById("user-email");
    let userMsg = document.getElementById("user-msg");
    let policyCheck = document.getElementById("policy");
    let form = document.getElementById("user-form");

    userName.value = localStorage.getItem("name");
    userEmail.value = localStorage.getItem("email");
    userMsg.value = localStorage.getItem("message");

    form.addEventListener("submit", async function (e) {
        let user = {
            name: userName,
            email: userEmail,
            policy: policyCheck,
            message: userMsg
        };
        let url = "https://formcarry.com/s/ODdm2t9bVz_";
        saveToLocalStorage();
        clearEntries();
        policyCheck.ariaChecked = false;
        let response = await fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(user)
        });

        // let result = await response.json();
        // alert(result.message);

        return false;
    });
}

function saveToLocalStorage() {
    let userName = document.getElementById("user-name").value;
    let userEmail = document.getElementById("user-email").value;
    let userMsg = document.getElementById("user-msg").value;
    localStorage.setItem("name", userName);
    localStorage.setItem("email", userEmail);
    localStorage.setItem("message", userMsg);
}

function clearEntries() {
    document.getElementById("user-name").value = "";
    document.getElementById("user-email").value = "";
    document.getElementById("user-msg").value = "";
}

function openPopUp() {
    window.history.replaceState({ popup: "open" }, "popup", "./popup");
    let popup = document.getElementById("auth-popup");
    popup.style.display = "flex";
    popup.style.alignItems = "center";
    popup.style.justifyContent = "center";
}

function closePopUp() {
    window.history.replaceState({ popup: "close" }, "popup", "./");
    let popup = document.getElementById("auth-popup");
    popup.style.display = "none";
}
